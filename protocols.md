# 6 Interaction-oriented programming

This chapter introduces a top-down approach to coordination, in which the focus is on the *interactions* among the application components. The most representative example of this kind of approach are communication protocols, often described with graphical languages such as message sequence charts or textual languages like Alice-and-Bob notations. This chapter uses a simple textual-based language for describing protocols and explains how to obtain distributed code from the protocol specification.

## 6.1 Interaction-oriented programming, protocols and choreographies

In interaction-oriented approaches the focus is on the interactions, i.e. the exchanges of messages, and how such interactions are organised in a global description that is usually called *protocol* or *choreography*. Protocols are conceived top-down: they describe the expected interactions in the protocol in terms of the messages exchanged between the protocol participants.

From a protocol description one can extract the coordination logic of each participant, to be used either for the actual implementation or as a specification of the role of the participant. The latter case is typical, for instance, of legacy systems (where existing implementations may be available) or open systems (where the principals may be governed by independent parties). In this chapter we focus on the former case, i.e. the generation of coordination logic for the participants, to be used in a distributed implementation of the protocol.

One of the advantages of interaction-oriented approaches is that desirable properties like deadlock-freedom or progress can, in some cases, be obtained by construction or through some static analysis of the protocol description.

## 6.2 A simple request-response protocol

The basic ingredient in a protocol description are interactions of the form

```
Alice.t -> Bob.x(T)
```

where `Alice` sends `Bob` the tuple `t`. Bob expects a tuple matching the template `T` and stores the result in the tuple variable `x`.

Such interactions can be composed in sequence. Let us see this in a pretty simple example of a *request-response* protocol where a user tries to login to a server:

```
   user.(username,password) -> server.credentials(string,string) ;
server.(check(credentials)) -> user.response(string)
```

The protocol is composed of two interactions: first, the `user` sends `username` and `password` to the server, who stores the result in a tuple of two strings called  `credentials`. Then, the server replies with the result of checking the credentials using the function `check`, which returns either `"ok"` or `"ko"`.

The protocol can be implemented by creating two channels (using a sequential space in pSpaces), one for each direction of the interactions: `channel(user,server)` and `channel(server,user)` and by programming each interaction depending on whether the participant is the sender or the receiver:

```
// User code
channel(user,server).put(username,password) ;
response := channel(server,user).get(string)

// Server code
credentials := channel(user,server).get(string,string) ;
channel(server,user).put(check(credentials))
```

The idea is that for an interaction from `A` to `B`, `A` puts data on channel `channel(A,B)` and `B` gets data from it. We will describe a code generation algorithm in more detail later in this chapter.

## 6.3 Request-response chains

The above simple example can be extended to chains of request-response interactions as in the following variant of the example, where the server asks an identity provider to check the credentials and forwards the response to the user:

```
                            user.(username,password) -> server.credentials(string,string) ;
server.(username(credentials),password(credentials)) -> identityProvider.credentials(string,string) ;
               identityProvider.(check(credentials)) -> server.response(string) ;
                                     server.response -> user.response(string)
```

The implementation is again pretty straightforward:

```
// User code
channel(user,server).put(username,password)
response := channel(user,server).get(string)

// Server code
credentials := channel(user,server).get(string,string)
channel(server,identityProvider).put(username(credentials),password(credentials))
response := channel(identityProvider,server).get(string,string)
channel(server,user).put(response)

// Identity Provider code
credentials := channel(server,identityProvider).get(string,string)
channel(identityProvider,server).put(check(credentials))
```

## 6.4 Breaking the chain

The interactions in the above examples form a chain, where every pair of consecutive interactions is of the form

```
   ... -> A...
  A... -> ...
```

i.e. the next sender is always the last receiver.

Sometimes, however, we need protocols that break such chain-chape. Consider as an example the following variant of our example, where the user sends the username to the server but it sends the password to the identity provider directly:

```
                             user.(username) -> server.username(string) ;
                             user.(password) -> identityProvider.password(string) ;
                           server.(username) -> identityProvider.username(string) ;
 identityProvider.(check(username,password)) -> server.response(string) ;
                             server.response -> user.response(string)
```

While this example is not problematic. In general, violating the chain shape may lead to protocols that cannot be implemented, do not behave as expected or that are ambiguous.

Consider for instance, the following protocol:

```
  Alice -> Bob  ;
Charlie -> Dave
```

In this protocol, there is no way to ensure that the interaction between Alice and Bob occurs before the interaction between Charlie and Dave, unless we introduce some hidden communication.

As an additional example, consider the protocol

```
Alice.u -> Bob.x(int) ;
Alice.v -> Bob.y(int)
```

This can also be problematic if the communication channel between Alice and Bob does not serialize messages: Bob could risk to store in `y` the value of `u` and in `x` the value of `v`.

There are several approaches address such issues:
* Impose well-formedness constraints on the structure of the protocol to avoid such problematic protocols.
* Implicitly add "hidden" interactions in the implementation so to ensure that interactions occur in the specified order.
* Relax the constraints imposed by the protocol and allow for certain deviations from the intended order of interactions.

## 6.5 Protocols with branches

The control flow of a protocol may not be linear and may contain branches that diverge depending on certain conditions. In the below example, a successful login phase is followed by a phase of the protocol in which the user gets some integer data from the server, whereas on failure to login the protocol simply terminates:

```
user.(username",password) -> server.credentials(string,string) ;
if check(credentials)@server then
    user.("getData") -> server.request("getData") ;
    server.(generateData()) -> user.data(int))
else
    server.("bye") -> user.response(string)
```

When implementing a protocol with a branch we need to ensure that all participants involved in any of the branches are informed of the branch to be taken. There should be a participant in charge of taking the decision on which branch to take and to inform the rest of the participants. The resulting code scheme would look as follows:

```
// User code
channel(user,server).put(username,password)
branch := channel(server,user).get(string)
if branch == "then" then
    channel(user,server).put("getData") ;
    data := channel(serve,user).get("data",int)
else
    channel(server,user).get("bye")

// Server code
credentials := channel(user,server).get(string,string)
if check(credentials) then
    channel(server,user).put("then") ;
    channel(user,server).get("getData") ;
    channel(server,user).put(generateData()) ;
else
    channel(server,user).put("else") ;
    channel(server,user).put("bye")
```

Participants that are in neither of the branches need not be notified.

## 6.6 Protocols with loops

Additional control flow constructs like loops can be considered as well. For instance, in the following variant of the above protocol, the user may keep requesting data until he decides to stop:

```
while notEnoughData()@user do {
    user.("getData") -> server.t("getData")
    server.(generateData()) -> user.data(string))
}
```

Again, one of the participants should be in charge of controlling the loop and notifying all involved participants of the branch (`continue` or `break`) to be taken. So the implementation would look like:

```
// User code
while true {
   if enoughData() then
       channel(user,server).put("continue")
       channel(user,server).put("getData") ;
       data := channel(server,user).get("data",string)
   else
       channel(user,server).put("break")
       break;
}

// Server code
while(true) {
    branch := channel(user,server).get(string)
    if branch == "continue" then
        channel(user,server).get("getData") ;
        channel(server,user).put(generateData()) ;
    else
        break
}
```

## 6.7 A simple protocol description language

Let us now put all the concepts together in a simple protocol description language, given by the following grammar:

```
P ::= s @ p                    // some local statement s performed by p
    | p.t -> q.x(T)            // interaction: p sends tuple t to q
                               // q expects a template T and stores the tuple in x
    | if c@p then P1 else P2   // a choice made by p
    | while c@p do P1          // a loop controlled by p
    | P1 ; P2                  // sequential composition
```

where
* `p`,`q` and `r` are protocol participants
* `c` is some boolean condition
* `s` is some local statement like an assignment, function or method invocation, etc.
* `t` is a tuple
* `T` is a template
* `x` is a tuple variable

## 6.9 Distributed implementation of a protocol

A distributed implementation for a protocol can be obtained by projecting the protocol on every participant and putting the resulting code together. The projection function `project(P,p)` that we define below describes how to generate the code that `p` needs to run to play his role in the protocol `P`.

The projection function for local actions is pretty simple:

```
project(s@p , p) = s
project(s@q , p) = ({})     if p != q
```

that is, `p` will execute the statement if the action specification states so, otherwise `p` will just execute an empty statement (denoted  `{}` in most programming languages).

The projection for interactions needs to consider whether `p` is the sender, the receiver or some other participant not involved in the interaction:

```
project(p.t -> q.T(x) , p) = channel(p,q).put(t)
project(p.t -> q.T(x) , q) = x := channel(p,q).get(T)
project(p.t -> q.T(x) , r) = {}                     // if r != p and r != q
```

The projection function for sequential composition is straightforward:

```
project(P1;P2 , p) = project(P1,p) ; project(P2,p)
```

The projection function for control flow constructs is slightly more involved. Let us start with `if_then_else`":

```
project(if c@p then P1 else P2 , p) = if c then
                                          for all q in (P1 or P2) do channel(p,q).put("then") ;
                                          project(P1,p)
                                      else
                                          for all q in (P1 or P2) do channel(p,q).put("else") ;
                                          project(P2,p)
project(if c@p then P1 else P2 , q) = branch := channel(p,q).get(string) ;         // if q in P1 or P2
                                      if branch == "then" then
                                          project(P1,q)
                                      else
                                          project(P2,q)
project(if c@p then P1 else P2 , r) = {}                                          // if q not in P1 or P2
```

The idea is that if `p` is in charge of checking the condition of the `if_then_else` and he must notify any other process `q` involved in the branches to go for one or the other branch. Participants not involved in that part of the protocol can just ignore the choice.

`While` loops are projected similarly:

```
project(while c@p do P1 , p) = while c do {
                                   for all q in P1 do channel(p,q).put("continue")
                                   project(P1,p)
                               }
                               for all q in P1 do channel(p,q).put("break")
pproject(while c@p do P1 , q) = while true {                                    // if q not in P1
                                  branch := channel(p,q).get(string);
                                  if branch == "continue" then project(P1,q)
                                  else break
                                }
project(while c@p do P1 , p) = {}                                               // if q not in P1
```

## 6.10 A complete example

We can now put the entire protocol description language at work in the following example:

```
user.(username,password) -> server.credentials(string,string) ;
server.(username(credentials),password(credentials)) -> identityProvider.credentials(string,string) ;
identityProvider.Put(check(credentials)) -> server.response(string) ;
if response == "ok" then
    while notEnoughData()@user do {
        user.("getData") -> server.t("getData")
        server.(generateData()) -> user.data(string))
    }
else
    server.("ko") -> user.response("ko")
```

## Summary

We have seen
* A simple interaction-oriented language for describing protocols
* Protocol patterns such as request-response, chains, branches and loops
* A projection function to generate distributed implementations of a protocol
