```
board |-> "fork"*"fork" |-
  board.get("fork"); board.get("fork"); board.put("fork"); board.put("fork")
‖ board.get("fork"); board.get("fork"); board.put("fork"); board.put("fork")

⇓

board |-> "fork" |-                                                                  board |-> nil |- 
  board.get("fork"); board.put("fork"); board.put("fork")                   =>       board.get("fork"); board.put("fork"); board.put("fork")
‖ board.get("fork"); board.get("fork"); board.put("fork"); board.put("fork")       ‖ board.get("fork"); board.put("fork"); board.put("fork")

⇓

board |-> nil |-
  board.put("fork"); board.put("fork")
‖ board.get("fork"); board.get("fork"); board.put("fork"); board.put("fork")

⇓

board |-> "fork" |-                                                                  board |-> nil |-
  board.put("fork")                                                          =>      board.put("fork")  
‖ board.get("fork"); board.get("fork"); board.put("fork"); board.put("fork")       ‖ board.get("fork"); board.put("fork"); board.put("fork")

⇓                                                                                    ||
                                                                                     ||
board |-> "fork"*"fork" |-                                                           ||
  board.get("fork"); board.get("fork"); board.put("fork"); board.put("fork")         ||
                                                                                     ||
⇓                                                                                    ||  
                                                                                     ||
board |-> "fork" |-                                                               <==//
  board.get("fork"); board.put("fork"); board.put("fork")

⇓

board |-> nil |-
  board.put("fork"); board.put("fork")

⇓

board |-> "fork" |-
  board.put("fork")

⇓

board |-> "fork"*"fork" |-
  0
