## Distributed programming with tuple spaces

This first group of three lectures provide the basic knowledge to get started programming distributed applications.

### 1. Programming with spaces
 * Preparation: get familiar with [pSpaces](https://github.com/pSpaces/) and read [tutorial 01](https://github.com/pSpaces/Programming-with-Spaces/blob/master/tutorial-tuple-spaces.md).
 * Content: [tutorial 01]( https://github.com/pSpaces/Programming-with-Spaces/blob/master/tutorial-tuple-spaces.md).
 * Exercises: Exercises 1.1-1.5 from the [exercise page](exercises.md).
 * Related courses at DTU: Database Systems [02170](http://kurser.dtu.dk/course/02170).

### 2. Concurrent programming
 * Preparation: read [tutorial 02](https://github.com/pSpaces/Programming-with-Spaces/blob/master/tutorial-concurrent-programming.md).
 * Content: [tutorial 02](https://github.com/pSpaces/Programming-with-Spaces/blob/master/tutorial-concurrent-programming.md).
 * Exercises: Exercises: 2.1-2.3 from the [exercise page](exercises.md).
 * Related courses at DTU: Concurrent Programming [02158](http://kurser.dtu.dk/course/02158).

### 3. Distributed programming
 * Preparation: read [tutorial 03](https://github.com/pSpaces/Programming-with-Spaces/blob/master/tutorial-distributed-programming.md).
 * Content: [tutorial 03](https://github.com/pSpaces/Programming-with-Spaces/blob/master/tutorial-distributed-programming.md).
 * Exercises: Exercises 3.1-3.3 in [exercise page](exercises.md).
 * Related courses at DTU: Distributed Systems [02220](http://kurser.dtu.dk/course/02220), Web Services [02267](http://kurser.dtu.dk/course/02267).

Some distributed programming patterns are:
* Client-server pattern: In the client-server pattern, we distinguish two kind of roles: *servers* (ready to offer some service) and *clients* (prompt to request services). Components with different roles tend to be asymmetric and exhibit differen behaviours, specialised for each role.  In an application a component can sometimes adopt the role of a server or of a client. For instance, in an example we will see later on, a server hosting some service to users may need to request authentication services to an identity provider system. Typically a server will interact with many clients, and a client with one or few servers. A server can thus become a *bottleneck* and a *single point-of-failure*. A possible way to mitigate this is to use server replication techniques. Typical examples of client-server applications we have seen in the lectures are the chat applications where we had a server to host and display the chat rooms and one client for each chat user. One of the examples uses a simple server replication technique in which each chat room was managed by an ad-hoc server process.  
* Peer-to-peer pattern: In the peer-to-peer pattern all components of the application, called *peers*, tend to be *equal* in terms of the behaviour they exibit and their role within the application. In principle, there is some sort of symmetry among the peers. The coordination of peer-to-peer applications is distributed and fully *decentralised*: there is no need of a central coordinator. Sometimes, however, there is a need to break symmetry and decentralisation, for example to initiate the application or to lead part of the coordination logic. Peer-to-peer applications tend to be more robust than client-server applications since there is no single point of failure. On the other hand, peer-to-peer may have more security concerns as we need to trust all peers or adopt techniques to control and mitigate malicious behaviours.  A typical example we have seen in the lectures are the dining philosophers: in that application all philosophers are peers running the same code and a central coordinator (the waiter) is only used to initiate the application.
* Actors: In this pattern, an application is conceived as a set of interacting *actors*. An actor is a computational entity with some internal state and with a special space called *inbox* that he uses to receive messages from other actors. The messages of an inbox can be read by the inbox's owner. An actor cannot send messages to its own inbox. The internal state of an actor is privated: actors can only interact through their inboxes. The general pattern for an actor is to react to the messages in the inbox, e.g. by changing their internal state, sending messages to other actors, or even create more actors. Actors can also have a proactive behaviour and they can be multi-threaded. Actor-based programming is supported by mainstream languages such as [Scala](http://www.scala-lang.org/old/node/242) and [Erlang](http://erlang.org/doc/getting_started/conc_prog.html), and by actor libraries such as [Akka](https://akka.io/).

## Understanding tuple spaces

This second group of lectures provide a deeper understanding on programming with tuple spaces.

### 4. Semantics of pSpace programs
 * Preparation: read the [formal semantics for a light version of pSpaces](https://github.com/pSpaces/Programming-with-Spaces/blob/master/semantics-light.md)  page.
 * Content: the [formal semantics for a light version of pSpaces](https://github.com/pSpaces/Programming-with-Spaces/blob/master/semantics-light.md)  page.
 * Exercises: Exercises 4.1-4.3 in [exercise page](exercises.md).
 * Related courses at DTU: Computer Science Modelling [02141](http://kurser.dtu.dk/course/02141).

### 5. Modelling and analysing pSpaces programs with Spin
 * Preparation: read about [how to use the Spin model checker to model and verify pSpace applications](https://github.com/pSpaces/Programming-with-Spaces/blob/master/Promela/promela.md).
 * Content: brief Spin tutorial and notes on [how to use the Spin model checker to model and verify pSpace applications](https://github.com/pSpaces/Programming-with-Spaces/blob/master/Promela/promela.md).
 * Exercises: Exercise 5.1-5.3 in [exercise page](exercises.md).
 * Related courses at DTU: Model Checking [02246](http://kurser.dtu.dk/course/02246).

# Coordination models

So far we have adopted a *process-oriented* approach to modelling and programming pSpace applications where applications are conceived as a set of processes (possibly running in different applications and hosts) that interact through tuple spaces. The focus is pretty much on programming the processes.  The process-oriented approach is a *bottom-up* approach to programming distributed applications: the application logic emerges from the composition of the processes. Distributed applications can also be modelled using *top-down* approaches where the global application logic is defined first. The coordination logic of the processes is then synthetised from that global logic. One advantage of *top-down* approaches is that, under certain conditions, the obtained application enjoys good properties (e.g. deadlock-freedom) by design, so that no verification is needed.

The next three chapters present a set of bottom-up approaches, explaining how to adopt them in pSpaces. In particular, we will consider:
 * *interaction-oriented* programming (protocols) where we focus on the interactions between a set of agents.
 * *task-oriented* programming (workflows) where we focus on tasks to be completed and the order among them.
 * *stream-oriented* programming (dataflows) where we focus on streams of data to be processesed by a network of data processing and routing units.

In the below chapters we often use the syntax of the [formal semantics](https://github.com/pSpaces/Programming-with-Spaces/blob/master/semantics-light.md) we saw in lecture 4 instead of actual code in Java or some other language, to simplify the presentation.

### 6. Interaction-oriented programming (protocols)
 * Preparation: read these [notes on interaction-oriented programming](protocols.md).
 * Content: [notes on interaction-oriented programming](protocols.md).
 * Exercises: Exercise 6.1 in [exercise page](exercises.md).

### 7. Task-oriented programming (workflows)
 * Preparation: read these [notes on task-oriented programming](workflows.md).
 * Content: [notes on task-oriented programming](https://gitlab.gbar.dtu.dk/02148/home/blob/master/workflows.md).
 * Exercises: Exercise 7.1 in [exercise page](exercises.md).

### 8. Stream-oriented programming (dataflows)
 * Preparation: read these [notes on stream-oriented programming](streams.md).
 * Content: [notes on stream-oriented programming](https://gitlab.gbar.dtu.dk/02148/home/blob/master/streams.md).
 * Exercises: Exercise 8.1 in [exercise page](exercises.md).

# Extra lectures

### 9. Secure Spaces
 * Preparation: read these [notes on secure tuple spaces](secure-spaces.md).
 * Content: [notes on secure tuple spaces](secure-spaces.md).
 * Exercises: Exercise 9.1 in [exercise page](exercises.md).
 * Related courses at DTU: Data Security [02239](http://kurser.dtu.dk/course/02239), Language-based Security [02244](http://kurser.dtu.dk/course/02244), Network Security [02233](http://kurser.dtu.dk/course/02233).
