# 8 Stream-oriented programming

Stream-oriented programming is a top-down approach to coordination where the focus is on routing and transforming data. We consider here a simple stream-oriented approach akin to dataflow programming and stream programming, where we conceive an application as a graph where nodes are data processing blocks that may produce, consume, transform and route data items (tuples), and where the edges relate the output stream of a block with the input stream of another block.

For the sake of simplicity we consider the following blocks only:
 * Producer: consumes no input and produces a stream of tuples.
 * Consumer: consumes a stream of tuples and produces nothing.
 * Transformer: consumes a stream and produces a new one, by transforming the incoming tuples.
 * Duplicator: consumes a stream and produces two copies of the stream.
 * Dispatcher: consumes a stream an produces two streams. Each input tuple is redirected into one of the two output streams according to some dispatching function.
 * Collector: consumes two streams and produces a stream that results by arbitrarily intertwining the input streams.
 * Merger: consumes two streams s1 and s2 and produces a stream where each tuple from s1 is merged together with a tuple from s2 according to some function.

 A dataflow can be implemented as a pSpace application. In the approach we sketch below we start creating a sequential space `stream(X,Y)` for every edge `X->Y` connecting block `X` with block `Y`. Every block `X` is then programmed as explained below.

## 8.1 A sensor dataflow example
As an example we consider a scenario where data produced by a set of sensors needs to be presented on two displays. Graphically, the dataflow has the following structure:

```
 sensor1 --\
           |
          merger --> duplicator --> aggregator --> display1
           |             |
 sensor2 --/             \--> display2
```

Sensors are redundant and are meant to measure the same value. Redundancy is used to ensure better quality of the measurement. The data produced by the sensors flows into a merger that computes the average value for each timestamp. The stream of data produced by the merger is then duplicated to two displays. One gets the data as it is, the other gets easier-to-digest data: to avoid overflowing the display with too much information, a transformer block takes care of consuming the stream and producing a new one where long sequences of values are compressed into a single (averaged) value.

## 8.2 Producers
For a producer A and the unique flow A->X the code of the producer is

```
while(true) {
    // produce the data
    ...
    stream(A,X).put(data);
}
```

## 8.3 Consumers
For a consumer A and the unique flow X->A the code of the consumer is

```
while(true) {
    data := stream(X,A).get(template);
    // do something with the data
    ...
}
```

## 8.4 Transformers
For a transformer A and the unique flows X->A , A->Y the code of the transformer based on a simple piece-wise transformation function `f` is

```
while (true) {
    data := stream(X,A).get(data);
    stream(A,Y).put(f(data))
}
```

## 8.5 Duplicators
For a duplicator A and the unique flows X->A , A->Y , A->Z the code is

```
while (true) {
    data := stream(A,X).get(template);
    stream(A,Y).put(data);
    stream(A,Z).put(data);
}
```

## 8.6 Displatchers
For a dispatcher A and the unique flows X->A , A->Y , A->Z the code is

```
while (true) {
    data := stream(X,A).get(template)
    if (// some routing condition here) {
        stream(A,Y).put(data)
    else {
        stream(A,Z).put(data)
        }
}
```

## 8.7 Collectors
A collector A with the unique flows X->A , Y->A , A->Z can be programmed as two parallel activities, each dealing with one of the input streams:

```
while (true) {
    data := stream(X,A).get(template)
    stream(A,Z).put(data)
}
```

and

```
while (true) {
    data := stream(Y,A).get(template)
    stream(A,Z).put(data)
}
```

## 8.8 Merger
For a merger A and the unique flows X->A , Y->A , A->Z the code is

```
while (true) {
    dataX := stream(X,A).get(templateX)
    dataY := stream(Y,A).get(templateY)
    stream(A,Z).put(merge(dataX,dataY))
}
```

where `merge` is some function that merges the data.

## Summary

We have seen a:
* A simple stream-oriented programming approach based on a basic set of data processing blocks
* A sketch of how to implemented such blocks using tuple spaces.
* An example of a sensor dataflow.
